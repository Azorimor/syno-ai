FROM node:18.17-slim AS frontendBuilder

WORKDIR /app/frontend
COPY ./frontend/package*.json ./
COPY ./frontend .

RUN npm install
RUN npm run build

FROM node:18.17-slim AS backendBuilder

WORKDIR /app/backend
COPY ./backend/package*.json ./
COPY ./backend .

RUN npm install
RUN npm run build

FROM node:18.17-slim AS final

RUN apt-get update \
    && apt-get install -y wget gnupg \
    && wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
    && sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' \
    && apt-get update \
    && apt-get install -y google-chrome-stable fonts-ipafont-gothic fonts-wqy-zenhei fonts-thai-tlwg fonts-kacst fonts-freefont-ttf libxss1 \
      --no-install-recommends \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /app/bot

ENV NODE_ENV=production
ENV PUPPETEER_SKIP_DOWNLOAD true

RUN npm install pm2 -g

COPY --from=frontendBuilder /app/frontend/dist /app/bot/frontend/dist
COPY --from=backendBuilder /app/backend/build /app/bot/backend/build 

WORKDIR /app/bot/backend
COPY --from=backendBuilder /app/backend/package*.json ./
RUN npm install --production \
  && groupadd -r botuser && useradd -r -g botuser -G audio,video botuser \
  && mkdir -p /home/botuser/Downloads \
  && chown -R botuser:botuser /home/botuser \
  && chown -R botuser:botuser /app/bot \
  && mkdir /app/bot/data \
  && chown -R botuser:botuser /app/bot/data

WORKDIR /app/bot
COPY ./ecosystem.config.js ./ecosystem.config.js

RUN chown -R botuser:botuser /app/bot

EXPOSE 8088
EXPOSE 8044

USER botuser

CMD [ "pm2-runtime", "start", "ecosystem.config.js" ]