import { z } from 'zod';

export const ChannelPostSchema = z.object({
  name: z.string().min(1),
  inToken: z.string().min(1),
  outToken: z.string().min(1)
});