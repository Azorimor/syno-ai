import { z } from 'zod';

export const UserSchema = z.object({
  id: z.number(),
  username: z.string().trim().min(1),
  user_id: z.number(),
  dsm_uid: z.number(),
  is_disabled: z.boolean(),
  nickname: z.string().min(1),
  uiAccess: z.boolean(),
});