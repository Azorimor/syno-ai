import { z } from 'zod';

export const SettingsSchema = z.object({
  id: z.number(),
  sslCert: z.string(),
  sslKey: z.string(),
  sslEnabled: z.boolean(),
  synoToken: z.string().min(1),
  openAIToken: z.string().min(1),
  synoUrl: z.string().url(),
  postOldContext: z.number().min(1),
  postMaxTokens: z.number().min(1),
  fakeUsername: z.string(),
  fakePassword: z.string(),
  systemMessage: z.string().min(1),
  openAiModel: z.string().min(1),
});