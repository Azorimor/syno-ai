import { DataSource } from 'typeorm';
import { Settings } from './entity/settings.entity';
import { Channel } from './entity/channel.entity';
import { User } from './entity/user.entity';
import { Message } from './entity/message.entity';
import { Session } from './entity/session';
import { OpenAi } from './entity/openai.entity';

export const AppDataSource = new DataSource({
  type: 'sqlite',
  database: './data/data.sqlite',
  synchronize: true,
  logging: process.env.NODE_ENV !== 'production',
  entities: [ Settings, Channel, User, Message, Session, OpenAi ],
  subscribers: [],
  migrations: [],
});
