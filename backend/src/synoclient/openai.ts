import { getSettings } from '../config';
import { encode } from 'gpt-3-encoder';
import { logger } from '../utils/logger';
import OpenAI, { ClientOptions } from 'openai';
import { ChatCompletion, ChatCompletionMessageParam } from 'openai/resources/chat/completions';

export class OpenAIClient {
  async generateText(
    messages: ChatCompletionMessageParam[]
  ): Promise<ChatCompletion | null> {
    try {
      const settings = await getSettings();
      const configuration: ClientOptions = {
        apiKey: settings.openAIToken,
      };
      const openai = new OpenAI(configuration);
      const params: OpenAI.Chat.ChatCompletionCreateParams = {
        model: settings.openAiModel,
        messages: messages,
      };
      const chatCompletion = await openai.chat.completions.create(params);
      const completed = chatCompletion;
      return completed;
    } catch (error) {
      logger.error(error, 'Could not complete the ai request.')
    }
    return null;
  }
  prepareContext(posts: {message: string}[], systemMessage: string) {
    const context : ChatCompletionMessageParam[] = [];
    context.push({
      role: 'system',
      content: systemMessage
    })
    for(const post of posts) {
      context.push({
        role: 'user',
        content: post.message,
      })
    }
    return context;
  }
  calculateTokenAmount(context: ChatCompletionMessageParam[]) {
    let totalAmount = 0;
    for (let i = 0; i < context.length; i++) {
      const msg = context[i];
      if(msg.content && typeof msg.content === 'string'){
        totalAmount += this.calculateTokenStringAmount(msg.content);
      }
    }
    return totalAmount;
  }
  calculateTokenStringAmount(message: string) {
    const encoded = encode(message);
    return encoded.length
  }
  reduceContextSize(context: ChatCompletionMessageParam[], maxTokens: number) {
    logger.debug('Reducing context size if neccessary.');
    let currentTokenNeeded = this.calculateTokenAmount(context);
    while(currentTokenNeeded > maxTokens && context.length > 1) {
      if (context[0].role === 'system') {
        const deleted = context.splice(1,2);
        logger.debug(deleted, 'Deleted from context. Skipping system message.');
      } else {
        const deleted = context.splice(0,1)
        logger.debug(deleted, 'Deleted from context.')
      }
      currentTokenNeeded = this.calculateTokenAmount(context)
    }
    logger.debug(context, 'Final context sent to openai.');
    return currentTokenNeeded
  }
}
