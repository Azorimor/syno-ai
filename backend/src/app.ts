import express from 'express';
import synologyRouter from './router/synology';
import adminRouter from './router/admin';
import cors from 'cors';
import { httpLogger } from './utils/logger';

const app = express()
if(process.env.API_LOGGER === 'true'){
  app.use(httpLogger);
}
app.use(cors())
app.use('/synology/', synologyRouter);
app.use('/admin/', adminRouter);

export default app;