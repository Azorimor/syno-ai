import dotenv from 'dotenv';
import { AppDataSource } from './dataSource';
import { Settings } from './entity/settings.entity';
import { User } from './entity/user.entity';

dotenv.config();

const ADMIN_UI_PATH = '../../../frontend/dist'
const HTTP_PORT = 8088;
const HTTPS_PORT = 8044;

export {
  HTTPS_PORT,
  HTTP_PORT,
  ADMIN_UI_PATH
}

class SettingsCache {
  private settings: Settings;

  async loadSettings() {
    const settings = await AppDataSource.manager.findOneBy(Settings, {
      id: 1,
    });
    if (settings !== null ) {
      this.settings = settings;
      return;
    }
    await this.createAndStoreDefaultSettings();
    await this.loadSettings()
  }

  private async createAndStoreDefaultSettings() {
    const settings = AppDataSource.getRepository(Settings).create({
      id: 1,
      sslCert: '',
      sslKey:  '',
      sslEnabled: false,
      synoToken: '',
      openAIToken: '',
      synoUrl: 'https://localhost:5001/webapi/entry.cgi?api=SYNO.Chat.External&version=2',
      postOldContext: 4,
      postMaxTokens: 500,
      isSetup: false,
      fakeUsername: '',
      fakePassword: '',
    })
    AppDataSource.getRepository(Settings).save(settings);
  
    const admin = AppDataSource.getRepository(User).create({
      id: -1,
      username: 'Admin',
      user_id: -1,
      dsm_uid: -1,
      is_disabled: false,
      nickname: 'Administrator',
      uiAccess: true,
    });
    AppDataSource.getRepository(User).save(admin);
    return settings;
  }

  public async getSettings() {
    if(!this.settings) {
      await this.loadSettings();
    }
    return this.settings;
  }

  public async updateSettings() {
    await this.loadSettings();
  }
}

const settingsCache = new SettingsCache();
export const getSettings = () => settingsCache.getSettings();
export const updateSettings = () => settingsCache.updateSettings();