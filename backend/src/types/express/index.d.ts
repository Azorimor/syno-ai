/* eslint-disable @typescript-eslint/no-explicit-any */
import {User} from '../../entity/user.entity';

export {}

declare global {
  namespace Express {
    export interface Request {
      user?: User;
      validatedBody?: any;
    }
  }
}