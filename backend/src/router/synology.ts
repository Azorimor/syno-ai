import express, { Router } from 'express';
import { receiveChannelMessage, receiveChatMessage } from '../controller/synology';
import verifySynoBotToken from '../middleware/verifySynoBotToken';

const router : Router = Router()

router.use(express.urlencoded({extended: true}));
router.post('/webhook', receiveChannelMessage);
router.use(verifySynoBotToken);

router.post('/receive', receiveChatMessage);

export default router;