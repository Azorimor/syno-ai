import express, { Router } from 'express';
import { ADMIN_UI_PATH } from '../config';
import path from 'path';
import { getSettings, patchSettings } from '../controller/settings';
import { deleteChannel, getChannels, postChannel } from '../controller/channel';
import authRouter from './auth';
import cookieParser from 'cookie-parser';
import verifySessionToken from '../middleware/verifySessionToken';
import analyticsRouter from './analytics';
import { validateSettingsUpdate } from '../middleware/settings.validator';
import userRouter from './users';
import { validateChannelPost } from '../middleware/channel.validator';


const router : Router = Router()

router.use('/ui/', express.static(path.join(__dirname , ADMIN_UI_PATH)));
router.get('/ui/*', (req, res) => res.sendFile(path.join(__dirname , ADMIN_UI_PATH, 'index.html')));


router.use(express.json());
router.use(cookieParser())

router.use('/auth', authRouter);

router.use(verifySessionToken);

router.get('/settings', getSettings);
router.patch('/settings', validateSettingsUpdate, patchSettings);

router.get('/channel', getChannels);

router.post('/channel', validateChannelPost , postChannel);
router.delete('/channel/:id', deleteChannel)

router.use('/analytics', analyticsRouter)
router.use('/user', userRouter)

export default router;