import { Router } from 'express';
import { getTokensPerDay } from '../controller/analytics';

const analyticsRouter : Router = Router()

analyticsRouter.get('/tokensperday', getTokensPerDay);

export default analyticsRouter;