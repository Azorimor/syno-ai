import { Router } from 'express';
import { deleteUser, getUsers, patchUser } from '../controller/user';
import { validateUserUpdate } from '../middleware/user.validator';

const userRouter : Router = Router()

userRouter.get('/', getUsers);
userRouter.patch('/:id', validateUserUpdate, patchUser);
userRouter.delete('/:id', deleteUser);

export default userRouter;