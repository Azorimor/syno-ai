import express, { Router } from 'express';
import { login, logout, requestLoginCode } from '../controller/auth';
import verifySessionToken from '../middleware/verifySessionToken';


const router : Router = Router()

router.use(express.json());

router.post('/login', login);
router.post('/code', requestLoginCode);

router.use(verifySessionToken);
router.post('/logout', logout);

export default router;