export function getBaseUrl(url: string) {
  try {
    const parsedUrl = new URL(url);
    return `${parsedUrl.protocol}//${parsedUrl.host}${parsedUrl.port}`
  } catch (error) {
    console.error('Invalid url. Cannot be parsed.');
  }
  return ''
}