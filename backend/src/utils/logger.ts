import pino from 'pino';
import httpPino from 'pino-http';

let logLevel = 'info'
if (process.env.LOG_LEVEL && process.env.LOG_LEVEL.length > 0) {
  logLevel = process.env.LOG_LEVEL
} else {
  logLevel = process.env.NODE_ENV !== 'production'? 'debug' : 'info'
}

const loggerOptions = {
  transport: {
    target: 'pino-pretty'
  },
  level: logLevel
}

export const logger = pino(loggerOptions);

export const httpLogger = httpPino(loggerOptions);
