import { HttpStatusCode } from 'axios';
import { Response } from 'express'

class ApiResponse {
  private success(res: Response, info: ApiInfo, data?: unknown) {
    res.status(info.code).json({
      status: 'success',
      info: info,
      data: data
    })
  }

  ok(res: Response, msg: string, data: unknown) {
    const info = {
      message: msg,
      code: HttpStatusCode.Ok,
      type: 'Ok'
    }
    this.success(res, info, data)
  }

  updated(res: Response) {
    const info = {
      message: 'Resource updated.',
      code: HttpStatusCode.Ok,
      type: 'Updated'
    }
    this.success(res, info);
  }

  private error(res: Response, err: ApiError) {
    res.status(err.code).json({
      status: 'failure',
      error: err
    })
  }

  notFound(res: Response, missing: string) {
    const error = {
      message: `Resource ${missing} not found.`,
      code: HttpStatusCode.NotFound,
      type: 'Not found',
      info: missing,
    }
    this.error(res, error);
  }

  unauthorized(res: Response) {
    const error = {
      message: 'You are not allowed to access this resource.',
      code: HttpStatusCode.Unauthorized,
      type: 'Unauthorized'
    }
    this.error(res, error)
  }

  badRequest(res: Response, validationError: unknown) {
    const error = {
      message: 'Your submitted data is in the wrong format.',
      code: HttpStatusCode.BadRequest,
      type: 'Bad Request',
      info: validationError,
    }
    this.error(res, error);
  }

  internalError(res: Response, info: string) {
    const error = {
      message: 'Internal Server Error',
      code: HttpStatusCode.InternalServerError,
      type: 'Internal Server Error',
      info: info
    }
    this.error(res, error);
  }
}

type ApiError = {
  message: string,
  code: number,
  type: string,
  info?: unknown
}

type ApiInfo = {
  message: string,
  code: number,
  type: string,
  info?: unknown
}

export default new ApiResponse();