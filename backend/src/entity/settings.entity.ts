import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('settings')
export class Settings {
  @PrimaryGeneratedColumn()
    id: number

  @Column()
    sslCert: string

  @Column()
    sslKey: string

  @Column()
    sslEnabled: boolean

  @Column()
    synoToken: string

  @Column()
    openAIToken: string

  @Column()
    synoUrl: string

  @Column()
    postOldContext: number

  @Column()
    postMaxTokens: number

  @Column()
    isSetup: boolean

  @Column({
    nullable: true
  })
    fakeUsername: string

  @Column({
    nullable: true
  })
    fakePassword: string

  @Column({
    default: 'You are a helpful assistant.'
  })
    systemMessage: string

  @Column({
    default: 'gpt-3.5-turbo'
  })
    openAiModel: string
}
