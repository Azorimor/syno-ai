import { BaseEntity, Column, CreateDateColumn, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { User } from './user.entity';

@Entity('session')
export class Session extends BaseEntity{

  @PrimaryGeneratedColumn()
    id: number

  @Column({
    unique: true,
    nullable: true,
    type: 'text',
  })
    token: string | null

  @Column({
    nullable: true,
    type: 'text'
  })
    requestToken: string | null

  @CreateDateColumn()
    createdAt: Date

  @UpdateDateColumn()
    updatedAt: Date

  @OneToOne(() => User, (user) => user.session)
  @JoinColumn()
    user: User
}