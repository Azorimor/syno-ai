import { Column, Entity, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Channel } from './channel.entity';
import { Session } from './session';

@Entity('user')
export class User {

  @PrimaryGeneratedColumn()
    id: number

  @Column({
    unique: true
  })
    username: string

  @Column({
    unique: true
  })
    user_id: number

  @Column()
    dsm_uid: number

  @Column()
    is_disabled: boolean

  @Column()
    nickname: string

  @Column({
    default: false
  })
    uiAccess: boolean

  @OneToMany(() => Channel, (channel) => channel.creator)
    channels: Channel[]

  @OneToOne(() => Session, (session) => session.user)
    session: Session
}