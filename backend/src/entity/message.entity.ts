import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('message')
export class Message {
  @PrimaryGeneratedColumn()
    id: number

  @Column()
    token: string

  @Column({
    nullable: true
  })
    channel_id: number
  
  @Column({
    nullable: true
  })
    channel_type: number

  @Column({
    nullable: true
  })
    channel_name: string
  
  @Column()
    user_id: number
  
  @Column()
    username: string
  
  @Column()
    post_id: number
  
  @Column()
    thread_id: number
  
  @Column()
    timestamp: number
  
  @Column()
    text: string
  
  @Column({
    nullable: true
  })
    trigger_word: string
}
