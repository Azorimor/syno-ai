import { Column, Entity, JoinTable, ManyToMany, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './user.entity';

@Entity('channel')
export class Channel {

  @PrimaryGeneratedColumn()
    id: number

  @Column({
    nullable: true,
    unique: true
  })
    channel_id: number

  @ManyToOne(() => User, (user) => user.channels)
    creator: User

  @Column()
    name: string

  @Column({
    nullable: true
  })
    purpose: string

  @ManyToMany(() => User)
  @JoinTable()
    members: User[]

  @Column({
    unique: true,
    nullable: true
  })
    inToken: string

  @Column({
    unique: true,
    nullable: true,
  })
    outToken: string
}