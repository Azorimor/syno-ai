import { Column, CreateDateColumn, Entity,  PrimaryGeneratedColumn } from 'typeorm';

@Entity('openai')
export class OpenAi {
  @PrimaryGeneratedColumn()
    id: number

  @CreateDateColumn()
    createdAt: Date

  @Column()
    content: string

  @Column()
    completionTokens: number

  @Column()
    totalTokens: number

  @Column()
    promptTokens: number

  @Column()
    model: string
}
