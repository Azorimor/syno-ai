import { Request, Response } from 'express';
import { AppDataSource } from '../dataSource';
import { User } from '../entity/user.entity';
import apiResponse from '../utils/apiResponse';

async function getUsers(req: Request, res: Response) {
  const users = await AppDataSource.manager.find(User);
  return apiResponse.ok(res, 'Getting all users' ,users);
}

async function patchUser(req: Request, res: Response) {
  const userId = Number(req.params.id);
  if (isNaN(userId))
    return apiResponse.badRequest(res, 'Param :id should be a valid number.');
  const result = await AppDataSource.getRepository(User).update({
    id: userId,
  }, req.validatedBody)

  if (result.affected && result.affected > 0) {
    return apiResponse.updated(res);
  }
  return apiResponse.notFound(res, `User with id ${userId}`)
}

async function deleteUser(req: Request, res: Response) {
  const userId = Number(req.params.id);

  if(isNaN(userId)) {
    return apiResponse.badRequest(res, 'Param :id should be a valid number.');
  }

  if(userId < 0) {
    return apiResponse.badRequest(res, 'Default admin account cannot be deleted.');
  }

  const deletedUser = await AppDataSource.manager.delete(User,{
    id: userId
  })
  if(deletedUser.affected && deletedUser.affected > 0){
    return apiResponse.updated(res);
  }
  return apiResponse.notFound(res, `User with id ${userId}`)
}

export {getUsers, patchUser, deleteUser}