import { Request, Response } from 'express';
import { AppDataSource } from '../dataSource';
import { Channel } from '../entity/channel.entity';
import apiResponse from '../utils/apiResponse';

async function getChannels(req: Request, res: Response) {
  const channels = await AppDataSource.manager.find(Channel);
  return apiResponse.ok(res, 'Returning all channels.', channels);
}

async function postChannel(req: Request, res: Response) {
  const payload = req.validatedBody;
  try {
    const createdChannels = await AppDataSource.manager.create(Channel, payload);
    await AppDataSource.manager.save(createdChannels);
    return apiResponse.ok(res, 'Added Channel', createdChannels);
  } catch (error) {
    return apiResponse.badRequest(res, 'Your submitted values are already in use or empty.');
  }
}

async function deleteChannel(req: Request, res: Response) {
  const channelId = Number(req.params.id);
  if (isNaN(channelId))
    return apiResponse.badRequest(res, 'Param :id should be a valid number.');

  const deletedChannel = await AppDataSource.manager.delete(Channel,{
    id: channelId
  })
  if(deletedChannel.affected && deletedChannel.affected > 0){
    return apiResponse.updated(res);
  }
  return apiResponse.notFound(res, `Channel with id ${channelId}`);
}

export {getChannels, deleteChannel, postChannel}