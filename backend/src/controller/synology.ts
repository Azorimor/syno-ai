import { Request, Response } from 'express';
import { SynoClient } from '../synoclient/synoclient';
import { OpenAIClient } from '../synoclient/openai';
import { AppDataSource } from '../dataSource';
import { Channel } from '../entity/channel.entity';
import { Message } from '../entity/message.entity';
import { User } from '../entity/user.entity';
import { getSettings } from '../config';
import { OpenAi } from '../entity/openai.entity';
import { logger } from '../utils/logger';
import { ChatCompletion, ChatCompletionMessageParam } from 'openai/resources/chat/completions';

const client = new SynoClient();
const aiClient = new OpenAIClient();

async function receiveChatMessage(req: Request, res: Response) {
  const settings = await getSettings();
  if(!settings.isSetup){
    res.sendStatus(200);
    return;
  }
  const correctChannelID = await client.getPrivateChannelID(Number(req.body.user_id));
  
  await insertUser(req.body);
  await storeMessage(req.body);
  await insertPrivateChannel(Number(req.body.user_id));

  
  const posts = await client.listPosts({
    post_id: Number(req.body.post_id),
    next_count: 0,
    prev_count: settings.postOldContext,
    channel_id: correctChannelID
  });
  const context = aiClient.prepareContext(posts, settings.systemMessage);
  const generatedMessage = await generateMessage(context);
  await client.sendMessage(Number(req.body.user_id), generatedMessage);

  res.sendStatus(200);
}

async function receiveChannelMessage(req: Request, res: Response) {
  const outToken = req.body.token;
  const settings = await getSettings();
  const channel = await AppDataSource.getRepository(Channel).findOneBy({
    outToken
  });
  const oldMessage = await AppDataSource.getRepository(Message).findAndCountBy({
    post_id: req.body.post_id
  });
  if(oldMessage[1] > 0){
    return res.sendStatus(200);
  }
  await storeMessage(req.body);
  await insertUser(req.body);

  const posts = [{message: req.body.text}];
  const context = aiClient.prepareContext(posts, settings.systemMessage);
  const generatedMessage = await generateMessage(context);

  await client.sendWebhookMessage(channel!.inToken, generatedMessage);
  res.sendStatus(200);
}

async function generateMessage(context: ChatCompletionMessageParam[]){
  if(process.env.NODE_ENV === 'production' || process.env.USE_AI === 'true'){
    const settings = await getSettings()
    const requiredTokenAmount = aiClient.reduceContextSize(context, settings.postMaxTokens)
    logger.debug(requiredTokenAmount)
    const generated = await aiClient.generateText(context)
    if(generated) {
      const generatedMessage = generated?.choices[0].message?.content;
      storeOpenAIResponse(generated)
      if(generatedMessage){
        logger.debug('Generated Message: ' + generatedMessage);
        return generatedMessage;
      }
    }
    return 'Application encounterd an error while generaten an ai response';
  }
  return 'Application is in development mode and does not contact OpenAi.'
}

async function storeOpenAIResponse(res: ChatCompletion){
  if(!res.usage){
    return;
  }
  const messageContent = res.choices[0].message?.content;
  const aiMessage = AppDataSource.getRepository(OpenAi).create({
    content: messageContent ? messageContent : undefined,
    completionTokens: res.usage?.completion_tokens,
    totalTokens: res.usage?.total_tokens,
    promptTokens: res.usage?.prompt_tokens,
    model: res.model,
  })
  return AppDataSource.getRepository(OpenAi).save(aiMessage)
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
async function storeMessage(content: any) {
  const newMessage = await AppDataSource.getRepository(Message).create(content);
  await AppDataSource.getRepository(Message).save(newMessage);
}

async function insertUser(payload: {user_id: number, username: string}){
  const userRepository = AppDataSource.getRepository(User);
  const existingUser = await userRepository.findOneBy({
    user_id: payload.user_id,
  });
  logger.debug(existingUser);
  if(existingUser) {
    return;
  }
  const newSynoUser = await client.getUserInfo(Number(payload.user_id));
  if(newSynoUser){
    const newUser = userRepository.create(newSynoUser);
    await userRepository.save(newUser);
    return newUser;
  }
}

async function insertPrivateChannel(user_id: number){
  const channel_id = await client.getPrivateChannelID(user_id);
  const synoChannel = await client.getChannelInfo(channel_id);
  const existingChannel = await AppDataSource.getRepository(Channel).findOneBy({
    channel_id
  });
  if(existingChannel){
    return;
  }
  const storedUser = await AppDataSource.getRepository(User).findOneBy({
    user_id: user_id
  });
  if(synoChannel && storedUser){
    const newChannel = new Channel();
    newChannel.channel_id = synoChannel.channel_id;
    newChannel.creator = storedUser;
    newChannel.name = synoChannel.name;
    newChannel.purpose = synoChannel.purpose;
    newChannel.members = [storedUser];
    const channel = await AppDataSource.manager.save(newChannel);
    return channel;
  }
}

export {receiveChatMessage, receiveChannelMessage, generateMessage};