import { Request, Response } from 'express';
import { AppDataSource } from '../dataSource';
import { Settings } from '../entity/settings.entity';
import { updateSettings } from '../config';
import apiResponse from '../utils/apiResponse';

async function getSettings(req: Request, res: Response) {
  const settings = await AppDataSource.manager.findOneBy(Settings, {
    id: 1,
  });
  return apiResponse.ok(res, 'Getting all settings.', settings);
}

async function patchSettings(req: Request, res: Response) {
  const settings = await AppDataSource.getRepository(Settings).findOneBy({
    id: 1,
  });
  if (!settings) {
    return apiResponse.internalError(res, 'Could not recieve settings to update.');
  }
  AppDataSource.getRepository(Settings).merge(settings, req.validatedBody,{isSetup: true});
  await AppDataSource.getRepository(Settings).save(settings);
  await updateSettings();
  return apiResponse.updated(res);
}

export { getSettings, patchSettings };
