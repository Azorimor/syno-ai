import { Request, Response } from 'express';
import { AppDataSource } from '../dataSource';
import { OpenAi } from '../entity/openai.entity';
import { logger } from '../utils/logger';

interface QueryTokensPerDay {
  day: string,
  totalTokens: number,
  promptTokens: number,
  completionTokens: number,
}

export async function getTokensPerDay(req: Request, res: Response) {
  const manager = AppDataSource.manager;
  const days = manager
    .createQueryBuilder()
    .from(OpenAi, 'o')
    .select('strftime("%Y-%m-%d", o.createdAt) AS day')
    .addSelect('*')
    .orderBy('o.createdAt', 'ASC');
  
  const results: QueryTokensPerDay[] = await manager
    .createQueryBuilder()
    .from(`(${days.getQuery()})`, 'days')
    .select('day')
    .addSelect('SUM(days.totalTokens)', 'totalTokens')
    .addSelect('SUM(days.promptTokens)', 'promptTokens')
    .addSelect('SUM(days.completionTokens)', 'completionTokens')
    .groupBy('day')
    .getRawMany();
  
  logger.debug(results, 'Results from custom query to get tokens per day.')

  return res.json(results);
}