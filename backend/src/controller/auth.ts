import { Request, Response } from 'express';
import { AppDataSource } from '../dataSource';
import { User } from '../entity/user.entity';
import { SynoClient } from '../synoclient/synoclient';
import { Session } from '../entity/session';
import { randomBytes } from 'crypto';
import { logger } from '../utils/logger';
import apiResponse from '../utils/apiResponse';

const LOGIN_CODE_SIZE = 9;
const MAX_REQUEST_CODE_LIFETIME_HOURS = 12;

//TODO Check what happens, when the user has not contacted the bot before in private channel
async function requestLoginCode(req: Request, res: Response) {
  const payload: {username: string} = req.body;
  const user = await AppDataSource.manager.findOneBy(User, {
    username: payload.username
  });
  if(!user) {
    return res.status(404).json({message: 'User not found.'});
  }
  const client = new SynoClient();
  if(user.id > 0 && !user.uiAccess) {
    client.sendMessage(user.user_id, 'You are *not allowed* to access the GUI. Contact administrator for access.')
    return apiResponse.unauthorized(res);
  }
  const code = generateLoginCode(LOGIN_CODE_SIZE);
  saveCode(code, user);

  if(user.id < 0){
    logger.info(`Login code for user ${payload.username} is: ${code}`);
  } else {
    client.sendMessage(user.user_id, `Enter the following code: *${code}*`);
  }
  return apiResponse.ok(res, 'Code successfully requested.', {});
}

async function login(req: Request, res: Response) {
  const payload : {username: string, code: string} = req.body;
  const user = await AppDataSource.getRepository(User).findOne({
    relations: {
      session: true,
    },
    where: {
      username: payload.username,
    },
  });
  if(!user || !user.session){
    return res.json({message: 'User not found or invalid code!'});
  }
  if (user.session.requestToken === payload.code) {
    const currentTime = new Date();
    const difference = currentTime.getTime() - user.session.updatedAt.getTime();
    if(difference > MAX_REQUEST_CODE_LIFETIME_HOURS * 3600000) {
      return res.json({message: 'The given code is to old.'});
    }
    const sessionToken = generateSessionToken();
    storeSessionToken(user.session.id, sessionToken);
    res.cookie('sessionToken', sessionToken, {
      maxAge: 1000 * 60 * 60 * 10,
      httpOnly: true,
      signed: false,
      sameSite: false,
    });
    return res.json({message: 'User login successfull', token: sessionToken, user: user });
  }
  return apiResponse.notFound(res, 'User');
}

async function logout(req: Request, res: Response) {
  const result = await AppDataSource.manager.delete(Session,{
    token: req.cookies.sessionToken,
  });
  if(result.affected === 0){
    res.json({message: 'Could not logout user.'})
  }
  res.json({}).status(200);
}

function generateLoginCode(size: number) {
  let number = '';
  for (let i = 0; i < size; i++) {
    number += Math.floor(Math.random()*10);
  }
  return number;
}

async function saveCode(code: string, user: User) {
  const storedUser = await AppDataSource.getRepository(User).findOne({
    relations: {
      session: true,
    },
    where: {
      username: user.username,
    },
  });
  if(!storedUser){
    return;
  }
  const session = storedUser.session;
  const partialUpdate = {
    requestToken: code,
    user: user,
    id: session ? session.id : undefined,
    token: null,
  };
  if(session) {
    await AppDataSource.getRepository(Session).update({
      id: session.id
    }, partialUpdate)
    return;
  }
  await AppDataSource.getRepository(Session).insert(partialUpdate);
}

function generateSessionToken(): string {
  const buffer = randomBytes(128);
  return buffer.toString('hex');
}

async function storeSessionToken(session_id: number, sessionToken: string) {
  await AppDataSource.manager.update(Session,{
    id: session_id
  }, {
    token: sessionToken,
    requestToken: null
  })
}

export {requestLoginCode, login, logout}