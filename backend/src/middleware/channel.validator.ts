import { ChannelPostSchema } from '../schema/channel.schema';
import { validate } from './validateDto';

export const validateChannelPost = validate(ChannelPostSchema.strip())