import { NextFunction, Request, Response } from 'express';
import { getSettings } from '../config';
import jsonResponse from '../utils/apiResponse';

async function verifySynoBotToken(req: Request, res: Response, next: NextFunction) {
  const token : string = req.body.token;
  const settings = await getSettings();
  const synoToken = settings.synoToken;
  if (token === synoToken || !settings.isSetup) {
    next();
  } else {
    jsonResponse.unauthorized(res);
  }
}

export default verifySynoBotToken;