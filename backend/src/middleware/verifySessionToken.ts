import { NextFunction, Request, Response } from 'express';
import { AppDataSource } from '../dataSource';
import { Session } from '../entity/session';
import jsonResponse from '../utils/apiResponse';

async function verifySessionToken(req: Request, res: Response, next: NextFunction) {
  const token : string = req.cookies.sessionToken;
  if(!token){
    jsonResponse.unauthorized(res);
    return;
  }
  const session = await AppDataSource.manager.findOne(Session, {
    relations: {
      user: true,
    },
    where: {
      token,
    },
  });
  if (session) {
    req.user = session!.user;
    next();
  } else {
    jsonResponse.unauthorized(res);
  }
}

export default verifySessionToken;