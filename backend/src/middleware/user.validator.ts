import { UserSchema } from '../schema/user.schema';
import { validate } from './validateDto';

export const validateUserUpdate = validate(UserSchema.partial().strip())