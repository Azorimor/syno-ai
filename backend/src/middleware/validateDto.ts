import { Request, Response, NextFunction } from 'express';
import { z } from 'zod';
import jsonResponse from '../utils/apiResponse';

export const validate =
  (schema: z.AnyZodObject | z.ZodOptional<z.AnyZodObject>) =>
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const validatedBody = await schema.parseAsync(req.body);
        if(isObjectEmpty(validatedBody)) {
          return jsonResponse.badRequest(res, 'Valid body is empty.');
        }
        req.validatedBody = validatedBody;
        next();
      } catch (error) {
        let err = error;
        if (err instanceof z.ZodError) {
          err = err.issues.map((e) => ({ path: e.path[0], message: e.message }));
        }
        return jsonResponse.badRequest(res, err);
      }
    };

function isObjectEmpty(obj: Record<string, unknown> | undefined): boolean {
  if (!obj) {
    return true;
  }
  return Object.keys(obj).length === 0;
}