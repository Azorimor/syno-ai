import { SettingsSchema } from '../schema/settings.schema';
import { validate } from './validateDto';

export const validateSettingsUpdate = validate(SettingsSchema.partial().strip())