import puppeteer, {Browser, BrowserContext, ElementHandle, Page} from 'puppeteer';
import { setTimeout } from 'timers/promises';
import { OpenAIClient } from '../synoclient/openai';
import {generateMessage} from '../controller/synology'
import { logger } from '../utils/logger';
import { getSettings } from '../config';

type ScreenMessage = {
  message: string,
  username: string | undefined,
  id: number,
}

export class SynoBrowserClient {
  url: string;
  browser: Browser;
  page: Page;
  context: BrowserContext;
  username: string;
  password: string;
  aiClient: OpenAIClient;
  
  constructor(url: string, username: string, password: string){
    this.url = url;
    this.username = username;
    this.password = password;
    this.aiClient = new OpenAIClient();
  }

  private async setup() {
    if (process.env.NODE_ENV !== 'production') {
      const headless = process.env.HEADLESS ? process.env.HEADLESS : false;
      this.browser = await puppeteer.launch({headless: headless, devtools: true});
    } else {
      this.browser = await puppeteer.launch({
        headless: 'new',
        executablePath: 'google-chrome-stable',
        args: ['--no-sandbox'] // required to run on synology docker otherwise: 
        // Failed to move to new namespace: PID namespaces supported, Network namespace supported, but failed: errno = Operation not permitted
        //[30:30:1018/163213.573142:FATAL:zygote_host_impl_linux.cc(201)] Check failed: . : Invalid argument (22)
        // works just fine without this args on other servers
      });
    }
    this.context = this.browser.defaultBrowserContext();
    this.context.overridePermissions(this.url, ['notifications']);
    this.page = await this.browser.newPage();
    this.page.setDefaultNavigationTimeout(20000)
    this.page.setViewport({width: 1280, height: 720});
    await this.page.setRequestInterception(true);
    this.page.on('request', (request) => {
      if (request.resourceType() === 'image' || 
          request.resourceType() === 'font' ||
          request.resourceType() === 'media') {
        request.abort();
      } else {
        request.continue();
      }
    });
  }

  public async login() {
    const usernameInputSelector = '#dsm-user-fieldset > div > div > div.input-container > input[type=text]';
    const passwordInputSelector = '#dsm-pass-fieldset > div.login-textfield-wrapper.password-field.field > div > div.input-container > input[type=password]';
    const buttonNextFormSelector = '#sds-login-vue-inst > div > span > div > div.login-body-section > div.login-tab-panel > div > div.tab-wrapper > div.tab-content-ct > div > div.login-tabs-content-wrapper.login-content-section > div.login-btn';
    const buttonKeepLoggedInSelector = '#dsm-pass-fieldset > div.login-checkbox'
    const buttonLoginSelector = '#sds-login-vue-inst > div > span > div > div.login-body-section > div.login-tab-panel > div > div.tab-wrapper > div.tab-content-ct > div > div.login-content-section > div.login-tabs-content-wrapper > div.login-btn';

    await this.setup();
    await this.page.goto(`${this.url}`);
    logger.info('Fake user inserting username');
    await this.page.waitForSelector(usernameInputSelector);
    await this.page.type(usernameInputSelector, this.username);
    await this.page.click(buttonNextFormSelector);
    logger.info('Fake user going to password input');
    await this.page.waitForSelector(usernameInputSelector);
    await setTimeout(1000);
    await this.page.waitForSelector(passwordInputSelector);
    logger.info('Fake user inserting password');
    await this.page.type(passwordInputSelector, this.password);
    await setTimeout(500);
    logger.info('Fake user keep logged in');
    await this.page.click(buttonKeepLoggedInSelector);
    logger.info('Fake user perform login')
    await this.page.click(buttonLoginSelector);
    //await this.page.waitForNavigation();
    await setTimeout(9000);
    logger.info('Fake user logged in')
  }

  private async goToChatStart() {
    await this.page.goto(`${this.url}/?launchApp=SYNO.SDS.Chat.Application#channels/1`);
  }

  public async waitForMessages() {
    await this.goToChatStart();
    logger.debug('starting waiting for messages');
    // eslint-disable-next-line no-constant-condition
    while (true) {
      try {
        logger.debug('starting individual scanning run.')
        const unreadChannelMessages = await this.searchUnreadChannelMessage();
        if(unreadChannelMessages){
          const aiText = await this.generateAiResponse(unreadChannelMessages);
          logger.debug(aiText);
        }
      } catch (error) {
        logger.error(error, 'Error appeared: ');
        logger.info('Will continue... ');
      }
    }
  }

  private async generateAiResponse(allMessages: ScreenMessage[]) {
    const settings = await getSettings();
    const context = this.aiClient.prepareContext(allMessages, settings.systemMessage);
    for(const ctx of context) {
      logger.debug(ctx.content, 'Context Content');
    }
    const generatedMessage = await generateMessage(context);
    await this.write(generatedMessage);

  }

  private async searchUnreadChannelMessage() {
    logger.debug('going to chat start')
    await this.goToChatStart();
    await setTimeout(5000);
    const unreadChannelSelector = '.highlight';
    logger.debug('waiting for unread selector')
    const unreadChannel = await Promise.race([
      this.page.waitForSelector(unreadChannelSelector),
      setTimeout(5000)
    ]);
    if(!unreadChannel){
      return;
    }
    logger.debug('finished waiting for unread selector')
    if(unreadChannel){
      await unreadChannel.click();
      await setTimeout(1000);
      const channelMessages = await this.readScreenMessages();
      logger.debug('parsedMessages: ', channelMessages);
      await setTimeout(1000);
      return channelMessages;
    }
  }

  private async readScreenMessages() {
    logger.debug('reading screen messages');
    const elements = await this.page.$$('.msg-wrap');
    logger.debug('found elements: ', elements);
    const parsedMessages: ScreenMessage[] = []
    for(const element of elements) {
      const id = await this.getMessageNodeId(element);
      for(const msg of parsedMessages) {
        if(msg.id === id) {
          //continue elementLoop;
        }
      }
      logger.debug(id, 'id');

      const textNode = await element.$$('.msg-text .markdown');
      logger.debug(textNode, 'textNode');
      const text = await textNode.at(0)?.evaluate((el) => el.textContent);
      logger.debug(text);

      const userNode = await element.$$('.msg-user-name');
      const username = await userNode.at(0)?.evaluate((el) => el.textContent);

      if(username && text){
        parsedMessages.push({
          id,
          username,
          message: text,
        })
      }
    }
    return parsedMessages;
  }

  //FIXME: Does not work currently. Wrong ids are returned.
  public async getMessageNodeId(element: ElementHandle<Element>) {
    const id = await element.evaluate(el => el.id);
    const matchResult = id.match(/^ext-gen(\d+)$/);

    if(matchResult && matchResult[1]) {
      const number = parseInt(matchResult[1], 10);
      return number;
    }
    return 0;
  }

  public async write(msg: string) {
    logger.debug('start writing')
    const chunkSize = 1900;
    for (let i = 0; i < msg.length; i += chunkSize) {
      const partialString = msg.slice(i, i + chunkSize);
      logger.debug(`Writing chunk ${i} with length ${partialString.length}.`)
      logger.debug(`chunk string: ${partialString}`)
      await this.writeMsg(partialString);
    }
  }

  public async writeMsg(msg: string) {
    logger.debug('writeMsg');
    const messageBoxSelector = 'div.chat-input-aria-main-v2 > div.msg-inputarea-textarea-wrap > div.chat-contenteditable-field';
    const buttonSendMessageSelector = 'div > div > div > span.msg-inputarea-send-btn > button';
    await this.page.waitForSelector(messageBoxSelector);
    await setTimeout(3000);
    logger.debug('trying to click on msg box');
    await this.page.click(messageBoxSelector);
    logger.debug('starting typing into the msg box');
    await setTimeout(1000);
    await this.page.type(messageBoxSelector, ':');
    await setTimeout(100);
    await this.page.evaluate((selector, value) => {
      const contentEditableElement = document.querySelector(selector) as HTMLElement;
      if (contentEditableElement) {
        contentEditableElement.innerText = value;
      } else {
        logger.error(`Element with selector ${selector} not found.`);
      }
    }, messageBoxSelector, msg);
    await this.page.click(messageBoxSelector);
    logger.debug('trying to click on the send button');
    await setTimeout(3000);
    await this.page.waitForSelector(buttonSendMessageSelector);
    await this.page.click(buttonSendMessageSelector);
    await setTimeout(1000);
  }
}