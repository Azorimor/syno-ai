import 'reflect-metadata'
import http from 'http';
import https from 'https';
import api from './app';
import { HTTPS_PORT, HTTP_PORT, getSettings } from './config';
import { AppDataSource } from './dataSource';
import { SynoBrowserClient } from './browser/synoBrowserClient';
import { getBaseUrl } from './utils/utils';
import { Settings } from './entity/settings.entity';
import { logger } from './utils/logger';


AppDataSource.initialize().then(async () => {
  const app = api;
  const settings = await getSettings();
  // HTTP Server starten
  const httpServer = http.createServer(app);
  httpServer.listen(HTTP_PORT, () => {
    logger.info('HTTP Server is running on port ' + HTTP_PORT)
  });

  if (settings.sslEnabled) {
    const httpsOptions = {
      key: settings.sslKey,
      cert: settings.sslCert,
    };

    try {
      const httpsServer = https.createServer(httpsOptions, app);
      httpsServer.listen(HTTPS_PORT, () => {
        logger.info('HTTPS Server is running on port ' + HTTPS_PORT);
      });
    } catch (err) {
      logger.error('Error while starting https server', err)
    }
  }
  try {
    await fakeUserSetup(settings);
  } catch (err) {
    console.log(err);
    logger.error('Error during Fake User setup.', err);
  }
}).catch((err) => {
  logger.error('Error during Data Source initialization', err);
});

async function fakeUserSetup(settings: Settings) {
  logger.info('Starting browser client setup');
  if(settings.fakeUsername.length === 0 || settings.fakePassword.length === 0) {
    logger.info('Credentials for the fake user are missing. Skipping setup.');
    return;
  }
  logger.info('Trying to connect fake user to server.');
  const baseUrl = getBaseUrl(settings.synoUrl);
  const sBrowserClient = new SynoBrowserClient(baseUrl, settings.fakeUsername, settings.fakePassword);
  await sBrowserClient.login();
  logger.info('Tried to login.');
  await sBrowserClient.waitForMessages();
}