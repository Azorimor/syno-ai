declare global {
  namespace NodeJS {
    interface ProcessEnv {
      NODE_ENV: 'development' | 'production';
      PORT?: string;
      SSL_CERT_KEY_PATH: string;
      SSL_CERT_PATH: string;
      SYNOLOGY_BOT_TOKEN: string;
      SSL_ENABLED: boolean;
      LOG_LEVEL: string;
      HEADLESS: boolean | 'new';
      USE_AI: string;
      API_LOGGER: string
    }
  }
}

// If this file has no import/export statements (i.e. is a script)
// convert it into a module by adding an empty export statement.
export {}