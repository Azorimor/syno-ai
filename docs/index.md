---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "Syno AI"
  text: "A Synology Chatbot to integrate OpenAI GPT"
  tagline: Documentation Work In Progress
  actions:
    - theme: brand
      text: User Guide
      link: /user-guide/getting-started
    - theme: alt
      text: Administration Guide
      link: /admin-guide/getting-started

features:
  - title: Web Admin Dashboard
    details: Change settings on the fly using the web dashboard. No need for editing config files.
  - title: Easy Deployment on Synology
    details: Easily deploy the bot on any Synology NAS using docker.
  - title: OpenAI Settings
    details: Change the model and limit the api quota usage by setting limits.
---

