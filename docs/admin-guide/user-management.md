# Managing Users
Users currently have the following settings.

## User Settings

### UI Access
If enabled, the user is allowed to access the gui. (This permission has no effect on the default Admin user).

**Default:** `false`