# Initial Configuration

## Admin Login
:::tip
For the admin login it is required to access the container logs of the bot, because the default user prints the access code to the console.
:::
1. Go to the login page in your browser. (f.e. `http://localhost:8088/admin/ui/login`).
2. Enter the default admin credentials (Username: `Admin`)
3. The login code is available in the container logs.

## User Login
:::warning
Currently there is no permission system. So every user, who has access to the ui can edit all settings.
:::
Before a DSM Synology Chat Account can be used to login to the ui the user needs to talk to the configured chat bot.
The initial [configuration](./configuration) can be done using the **Admin Login**.
After that the following steps are required to enable the ui access for one specifig user.
1. The user first needs to write any message to the configured synology **bot**.
   It needs to be any message to the bot and not to the fake user, if a fake user was enabled.
2. Any user, who has already access to the ui (for example the default admin user) can log into the ui and enable the ui access for the other user. See also [user management](./user-management).
3. Now the user can just request a login code, which then gets sent to the user.

User logins are valid for **10 hours** and the login codes are valid up to **12 hours**, if they are not used.