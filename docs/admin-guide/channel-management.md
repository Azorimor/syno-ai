# Manage Channels
:::info
This settings are using webhooks. It requires some setup. It is easier to use the **fake synology user** and add this user to channels in order to integrate the chatbot in them.
:::