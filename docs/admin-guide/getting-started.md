---
outline: deep
---
# Getting started

## Installation
To install the bot it is recommended to use docker. The container does work with Synology Docker (Container Manager).
You can use the following `docker-compose.yml` content to run the container.

```yml
version: '3'

services:
  bot:
    image: codeberg.org/azorimor/syno-ai:latest
    restart: unless-stopped
    ports:
      - 8088:8088
      - 8044:8044
    volumes:
      - data:/app/bot/data

volumes:
  data:
```
Using this configuration, the data is managed by docker (because Synology uses strange permission system for their file system).
The Web UI is accessable on port `8088` using HTTP and on port `8044` using HTTPS.
To use the UI go to `http://localhost:8088/admin/ui`.
You can now make the [configuration](./configuration).