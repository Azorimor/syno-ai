# Initial Configuration
All settings can be edited on the web ui. See how to [login](./login) the first time.

## Settings
For the initial configuration it is recommended to set up the bot to connect to the Synology Chat server.
:::warning
Do not forget to click the **Save** Button at the end of the page.
:::

### Fake Username & Password
:::info
This user is required to integrate the bot as normal user to the chat application.
:::
This is the username & password for the fake OpenAI Bot user. This is a normal DSM user account, who has access to DSM and the Synology Chat Application.

### Synology Bot Token
This is the synology chat bot token provided by the synology chat application. This is required for the bot integration, which is used to send login codes.

### Synology Base URL
This is the base URL for the synology chat application. 

**Example:** `https://172.17.0.1:5001/webapi/entry.cgi?api=SYNO.Chat.External&version=2`
### Max number of old posts to generate the context
This value represents the maximum number of messages sent to OpenAI on each request.

**Default:** `4`
### Max number of request tokens
This value is used as maximum allowed tokens for one request to OpenAI. It calculates the amount of tokens required to answer the given question and only includes up to the defined value on tokens.

**Default:** `500`
:::warning
The default system message setting is always included in the request to OpenAI. So don't set this value to low or the message to long.
:::
### AI Model
The AI Model to use. Currently `GPT-3.5` and `GPT-4` are used.

### Default System Message
This message gets added to every request to OpenAI as `system` message type.

**Example:** `You are a great and happy assistant.`
### SSL Certificate & SSL Key & SSL Enabled
This fields are required for the ssl setup. If you are not using a reverse proxy, it is recommended to use HTTPS insted of HTTP.