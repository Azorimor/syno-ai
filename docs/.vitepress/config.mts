import { defineConfig } from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "Syno AI",
  description: "A Synology Chatbot to integrate OpenAI GPT",
  base: '/syno-ai',
  sitemap: {
    hostname: 'https://azorimor.codeberg.page/syno-ai/',
    lastmodDateOnly: false
  },
  head: [
    [
      'meta', {name: 'google-site-verification', content: 'SvnQzKoO-taAtMulB10rszlwfdzW1s_2Cz2h45tGdm4'}
    ]
  ],
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: 'Home', link: '/' },
    ],
    search: {
      provider: 'local'
    },

    sidebar: [
      {
        text: 'User Guides',
        items: [
          { text: 'Getting started', link: '/user-guide/getting-started' },
        ]
      },{
        text: 'Admin Guides',
        items: [
          { text: 'Getting started', link: '/admin-guide/getting-started' },
          { text: 'Login', link: '/admin-guide/login' },
          { text: 'Configuration', link: '/admin-guide/configuration' },
          { text: 'Manage Users', link: '/admin-guide/user-management' },
          { text: 'Dashboard', link: '/admin-guide/dashboard' },
          { text: 'Manage Channels', link: '/admin-guide/channel-management' },
        ]
      }
    ],

    socialLinks: [
      { icon: 'github', link: 'https://codeberg.org/azorimor/syno-ai' }
    ]
  }
})
