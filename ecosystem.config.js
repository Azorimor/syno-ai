module.exports = {
  apps : [{
    script: 'backend/build/index.js',
    name: 'syno-ai',
    exec_mode: 'cluster',
    instances: 1
  }],
};
