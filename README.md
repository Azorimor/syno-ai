![Translation status](https://translate.codeberg.org/widget/syno-ai/app/svg-badge.svg)
# Syno-AI
Custom Chatbot for Synology Chat to integrate OpenAI language models like GPT-3.5 and GPT-4.

## Features
- Web GUI for managing users, settings and statistics
- Enable the openai bot for all users or limit the access to specific users
- Specify the token limit of the context (to save on openai tokens -> save money)
- Specify the last message limit for the context (to save on openai tokens -> save money)
- Support for some public chat channels
- Simple docker deployment
- Deployable on a Synology NAS using docker without command line configurations
- Support for HTTPS without reverse proxy if required
- Multiple languages for the web dashboard

## Quickstart
1. Download the `docker-compose.yml` file.
2. Run the file using `docker-compose up -d`.
3. Goto `http://localhost:8088/admin/ui` to access the web interface.
4. Use the default admin credentials. Goto `/login` and enter `Admin` as username and copy the generated code from the container logs.
5. Enter all settings in the ui at `/settings`.
6. To get an access token as user, the user needs to write a message to the configured bot. This needs to be a message, which is directy send to the bot and not the fake user.

## Documentation
The documentation is available [here](https://azorimor.codeberg.page/syno-ai/). It is still work in progress.