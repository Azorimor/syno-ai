import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.tsx'
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import './index.css'
import { PersistGate } from "redux-persist/integration/react";
import { Provider } from 'react-redux';
import { persistor, store } from './store.ts';
import Login from './routes/login.tsx';
import Dashboard from './routes/dashboard.tsx';
import Settings from './routes/settings.tsx';
import Channels from './routes/channels.tsx';
import { Loading } from './components/Loading.tsx';
import Users from './routes/users.tsx';
import './i18n.ts';
import Profile from './routes/profile.tsx';

const router = createBrowserRouter([
  {
    path: "/",
    element: <App/>,
    children: [
      {
        path: '/login',
        element: <Login/>
      },
      {
        path: '/dashboard',
        element: <Dashboard/>
      },
      {
        path: '/settings',
        element: <Settings/>
      },
      {
        path: '/channels',
        element: <Channels/>
      },
      {
        path: '/users',
        element: <Users/>
      },
      {
        path: '/profile',
        element: <Profile/>
      }
    ]
  },
], {
  basename: "/admin/ui"
});

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <Provider store={store}>
      <PersistGate loading={<Loading/>} persistor={persistor}>
        <RouterProvider router={router} />
      </PersistGate>
    </Provider>
  </React.StrictMode>,
)