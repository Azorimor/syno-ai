import { NavigationBar } from "./components/NavigationBar"
import { Toaster } from "./components/ui/toaster.tsx"

interface LayoutProps {
  children: React.ReactNode
}

export default function Layout({children} : LayoutProps) {
  return (
    <>
      <NavigationBar/>
      <div className="w-4/5 m-auto pt-4">
        {children}
      </div>
      <Toaster/>
    </>
  )
}