import { Outlet } from "react-router";
import "./App.css";
import Layout from "./layout";
import { ThemeProvider } from "./components/ThemeProvider";

function App() {
  return (
    <>
      <ThemeProvider>
        <Layout>
          <Outlet/>
        </Layout>
      </ThemeProvider>
    </>
  );
}

export default App;
