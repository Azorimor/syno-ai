import { emptySplitApi } from "@/services/emptySplitApi";
import { combineReducers } from "@reduxjs/toolkit";
import authReducer from "./auth/authSlice";
import settingsReducer from "./settings/settingsSlice";

export const appReducer = combineReducers({
  [emptySplitApi.reducerPath]: emptySplitApi.reducer,
  auth: authReducer,
  settings: settingsReducer
})