import { User } from "@/services/auth"
import { RootState } from "@/store"
import { PayloadAction, createSlice } from "@reduxjs/toolkit"
import storage from "redux-persist/lib/storage"

interface AuthState  {
  user: User | null
  code: string | null
  token: string | null
  codeSuccess: boolean
  lastLogin: number
}

const initialState: AuthState = {
  user: null,
  code: null,
  token: null,
  codeSuccess: false,
  lastLogin: 0
}

const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setUser: (
      state,
      { payload: { user } }: PayloadAction<{ user: User | null}>
    ) => {
      state.user = user
      state.lastLogin = new Date().getTime()
    },
    logoutUser: (
      state
    ) => {
      state.user = null
      state.lastLogin = 0
      storage.removeItem('persist:root')
    },
    setToken: (
      state,
      { payload: {token}}: PayloadAction<{token: string}>
    ) => {
      state.token = token
    },
    setCode: (
      state,
      { payload: {code}}: PayloadAction<{code: string}>
    ) => {
      state.code = code
    },
    setCodeSuccess: (
      state
    ) => {
      state.codeSuccess = true
    },
  },
})

export const { setUser, setToken, setCode, setCodeSuccess, logoutUser } = authSlice.actions

export default authSlice.reducer

export const selectCurrentUser = (state: RootState) => state.auth.user
