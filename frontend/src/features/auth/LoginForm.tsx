import { Button } from "@/components/ui/button";
import { Form, FormControl, FormField, FormItem, FormLabel, FormMessage } from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { cn } from "@/lib/utils";
import { zodResolver } from "@hookform/resolvers/zod";
import { SyntheticEvent } from "react";
import { useForm } from "react-hook-form";
import { z } from "zod";
import { useLoginMutation, useRequestCodeMutation } from "@/services/auth";
import { useAppDispatch, useAppSelector } from "@/hooks/store";
import { setCodeSuccess, setToken, setUser } from "./authSlice";
import { useNavigate } from "react-router";
import { useTranslation } from "react-i18next";
import { InfoTooltip } from "@/components/InfoTooltip";

interface LoginFormProps extends React.HTMLAttributes<HTMLDivElement>{}

const loginFormSchema = z.object({
  username: z.string().nonempty(),
  code: z.string().nonempty().length(9)
})

type LoginFormValues = z.infer<typeof loginFormSchema>

const defaultValues: Partial<LoginFormValues> = {
  username: "",
  code: ""
}

export function LoginForm({className, ...props}: LoginFormProps) {
  const { t } = useTranslation()
  const navigate = useNavigate()
  const dispatch = useAppDispatch()

  const [code, {isLoading: isCodeLoading}] = useRequestCodeMutation()
  const [login, {isLoading: isLoginLoading}] = useLoginMutation()

  const authData = useAppSelector(state => state.auth)

  const form = useForm<LoginFormValues>({
    resolver: zodResolver(loginFormSchema),
    defaultValues,
  })

  async function onSubmit(data: LoginFormValues) {
    try {
      const loginTry = await login(data).unwrap()
      console.log(loginTry)
      dispatch(setToken(loginTry))
      if(loginTry.user){
        dispatch(setUser(loginTry))
      }
      navigate('/dashboard')
    } catch (error) {
      console.log(error);
    }
  }

  async function onRequestCode(event: SyntheticEvent) {
    event.preventDefault();
    try {
      const codeRequested = await code(form.getValues()).unwrap()
      if(codeRequested.message.length > 20){ //TODO Add correct check here
        dispatch(setCodeSuccess())
      }
    } catch (error) {
      console.log(error)
    }
  }

  return ( 
    <div className={cn("grid gap-6", className)} {...props}>
      <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)}>
        <FormField
          control={form.control}
          name="username"
          render={({field}) => (
            <FormItem className="mb-4">
              <FormLabel>{t('login.username.name')}<InfoTooltip info={t('login.username.description')}/></FormLabel>
              <FormControl>
                <Input
                  id="username"
                  placeholder="user25" {...field}
                  type="text"
                  autoCapitalize="none"
                  autoComplete="text"
                  autoCorrect="off"
                  disabled={authData.codeSuccess && !isCodeLoading}
                />
              </FormControl>
              <FormMessage/>
            </FormItem>
          )}
        />
        <div className="grid gap-2">
          <Button disabled={isCodeLoading} onClick={onRequestCode} className={authData.codeSuccess && !isCodeLoading ? "hidden" : ""}>
            {isCodeLoading && (
              <p>Loading..</p>
            )}
            {t('login.requestCode')}
          </Button>
        </div>
        <div className={!authData.codeSuccess ? "hidden" : ""}>
          <FormField
            control={form.control}
            name="code"
            render={({field}) => (
              <FormItem className="mb-4">
                <FormLabel>{t('login.code.name')}<InfoTooltip info={t('login.code.description')}/></FormLabel>
                <FormControl>
                  <Input
                    id="code"
                    placeholder="123456789" {...field}
                    type="password"
                    autoCapitalize="none"
                    autoComplete="text"
                    autoCorrect="off"
                    disabled={isLoginLoading}
                  />
                </FormControl>
                <FormMessage/>
              </FormItem>
            )}
          />
          <div className="grid gap-2">
            <Button disabled={isLoginLoading}>
              {isLoginLoading && (
                <p>Loading..</p>
              )}
              {t('login.login')}
            </Button>
          </div>
        </div>
      </form>
    </Form>
    </div>
  )
}