import { RootState } from "@/store"
import { PayloadAction, createSlice } from "@reduxjs/toolkit"

interface SettingsState  {
  locale: string,
  theme: 'dark' | 'light' | 'system'
}

const initialState: SettingsState = {
  locale: 'en',
  theme: 'light'
}

const settingsSlice = createSlice({
  name: 'settings',
  initialState,
  reducers: {
    setLocale: (
      state,
      { payload: {locale}}: PayloadAction<{locale: string}>
    ) => {
      state.locale = locale
    },
    setTheme: (
      state,
      { payload: {theme}}: PayloadAction<{theme: 'dark' | 'light' | 'system'}>
    ) => {
      state.theme = theme
    },
  },
})

export const { setLocale, setTheme } = settingsSlice.actions

export default settingsSlice.reducer

export const selectCurrentLocale = (state: RootState) => state.settings.locale

export const selectCurrentTheme = (state: RootState) => state.settings.theme