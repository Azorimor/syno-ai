import { Loader2 } from 'lucide-react';


export function Loading() {
  return (
    <>
      <div>
        <Loader2 className='mr-2 h-16 w-16 animate-spin'/>
      </div>
    </>
  )
}