export function PageTitle({title}: {title: string}) {
  return (
    <>
      <h1 className="text-3xl font-bold tracking-tight pb-4">
      {title}
      </h1>
    </>
  )
}