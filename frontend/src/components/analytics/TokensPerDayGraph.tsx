
import { Card, CardHeader, CardTitle, CardDescription, CardContent } from "../ui/card";
import { TokensPerDay, useGetTokensPerDayQuery } from "@/services/analytics";
import { CalendarDatum, ResponsiveCalendar } from '@nivo/calendar'
import { Loading } from "../Loading";

export function TokensPerDayGraph() {
  const {data, isLoading} = useGetTokensPerDayQuery()

  let finalData: CalendarDatum[] = []
  const keys: string[] = []
  if(data){
    for (const d of data) {
      keys.push(d.day);
    }
    finalData = transformTokensPerDayToBarDatum(data);
  }
  function transformTokensPerDayToBarDatum(tokensPerDay: TokensPerDay[]): CalendarDatum[] {
    return tokensPerDay.map((data) => ({
      day: data.day,
      value: data.totalTokens,
    }));
  }

  return (
    <>
      <Card className="">
        <CardHeader>
          <CardTitle>Tokens Per Day</CardTitle>
          <CardDescription>Tokens used per day</CardDescription>
        </CardHeader>
          <CardContent>
            { isLoading ? <Loading/> : 
            <div className="h-[400px]">
                <ResponsiveCalendar
                  data={finalData}
                  from="2023-01-01"
                  to="2024-06-30"
                  emptyColor="#ddd"
                  colors={[ '#61cdbb', '#97e3d5', '#e8c1a0', '#f47560' ]}
                  margin={{ top: 40, right: 40, bottom: 40, left: 40 }}
                  yearSpacing={40}
                  monthBorderColor="#ffffff"
                  dayBorderWidth={2}
                  dayBorderColor="#ffffff"
                  legends={[
                      {
                          anchor: 'bottom-right',
                          direction: 'row',
                          translateY: 36,
                          itemCount: 4,
                          itemWidth: 42,
                          itemHeight: 36,
                          itemsSpacing: 14,
                          itemDirection: 'right-to-left'
                      }
                  ]}
                />
            </div> 
            }
          </CardContent>
        </Card>
    </>
  )
}