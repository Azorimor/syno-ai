import { Channel, useDeleteChannelMutation, useUpdateChannelMutation } from "@/services/channel";
import { Button } from "./ui/button";
import { Card, CardHeader, CardTitle, CardDescription, CardContent, CardFooter } from "./ui/card";
import { Input } from "./ui/input";
import { z } from "zod";
import { useToast } from "./ui/use-toast";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { Form, FormControl, FormField, FormItem, FormLabel } from "./ui/form";
import { SyntheticEvent } from "react";
import { useTranslation } from "react-i18next";
import { InfoTooltip } from "./InfoTooltip";

const channelFormSchema = z.object({
  name: z.string().nonempty(),
  inToken: z.string(),
  outToken: z.string(),
});

type ChannelFormValues = z.infer<typeof channelFormSchema>

const defaultValues: Partial<ChannelFormValues> = {
  name: "",
  inToken: "",
  outToken: "",
}

export function ChannelCard({id, name, inToken, outToken}: Channel) {
  const { t } = useTranslation()

  const [updateChannel] = useUpdateChannelMutation()
  const [deleteChannel] = useDeleteChannelMutation()

  const { toast } = useToast()

  const form = useForm<z.infer<typeof channelFormSchema>>({
    resolver: zodResolver(channelFormSchema),
    defaultValues: defaultValues,
    values: {
      name,
      inToken: inToken ? inToken : "",
      outToken: outToken ? outToken : "",
    }
  });

  async function onSubmit(values: z.infer<typeof channelFormSchema>){
    try {
      const updateTry = await updateChannel({id, ...values}).unwrap()
      if(updateTry){
        toast({
          title: "Successfully updated.",
          description: "Channel successfully updated!"
        })
      }
    } catch (error) {
      console.log(error);
      toast({
        title: "Error",
        description: "Error while updating the channel information.",
        variant: "destructive",
      })
    }
  }

  async function onDelete(event: SyntheticEvent) {
    console.log(event)
    event.preventDefault()
    try {
      if(id){
        const deleteTry = await deleteChannel(id).unwrap()
        console.log(deleteTry);
        if(deleteTry){
          toast({
            title: "Successfully deleted.",
            description: "Channel successfully deleted!"
          })
        }
      }
    } catch (error) {
      console.log(error);
      toast({
        title: "Error",
        description: "Error while deleting the channel information.",
        variant: "destructive",
      })
    }
  }

  return (
    <>
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)}>
        <Card className="">
          <CardHeader>
            <CardTitle>{name}</CardTitle>
            <CardDescription>{id}</CardDescription>
          </CardHeader>
          <CardContent>
                <FormField
                  control={form.control}
                  name="name"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>{t('channels.name.name')}<InfoTooltip info={t('channels.name.description')}/></FormLabel>
                      <FormControl>
                        <Input
                          id="name"
                          type="text"
                          autoCapitalize="none"
                          autoComplete="text"
                          autoCorrect="off"
                          {...field}
                        />
                      </FormControl>
                    </FormItem>
                  )}
                />
                <FormField
                  control={form.control}
                  name="inToken"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>{t('channels.inToken.name')}<InfoTooltip info={t('channels.inToken.description')}/></FormLabel>
                      <FormControl>
                        <Input
                          id="inToken"
                          type="text"
                          autoCapitalize="none"
                          autoComplete="text"
                          autoCorrect="off"
                          {...field}
                        />
                      </FormControl>
                    </FormItem>
                  )}
                />
                <FormField
                  control={form.control}
                  name="outToken"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>{t('channels.outToken.name')}<InfoTooltip info={t('channels.outToken.description')}/></FormLabel>
                      <FormControl>
                        <Input
                          id="outToken"
                          type="text"
                          autoCapitalize="none"
                          autoComplete="text"
                          autoCorrect="off"
                          {...field}
                        />
                      </FormControl>
                    </FormItem>
                  )}
                />
          </CardContent>
            <CardFooter className="flex justify-between">
              {id ? <Button variant="destructive" onClick={onDelete}>{t('delete')}</Button> : <></>}
              <Button>{t('update')}</Button>
            </CardFooter>
          </Card>
        </form>
      </Form>
    </>
  )
}