import { Button } from "./ui/button";
import { Card, CardHeader, CardTitle, CardDescription, CardContent, CardFooter } from "./ui/card";
import { z } from "zod";
import { useToast } from "./ui/use-toast";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { Form, FormControl, FormField, FormItem, FormLabel } from "./ui/form";
import { SyntheticEvent } from "react";
import { User, useDeleteUserMutation, useUpdateUserMutation } from "@/services/user";
import { Switch } from "./ui/switch";
import { useTranslation } from "react-i18next";
import { InfoTooltip } from "./InfoTooltip";

const userFormSchema = z.object({
  username: z.string().trim().min(1),
  is_disabled: z.boolean(),
  nickname: z.string().min(1),
  uiAccess: z.boolean(),
});

type UserFormValues = z.infer<typeof userFormSchema>

const defaultValues: Partial<UserFormValues> = {
  username: 'Loading',
  is_disabled: false,
  nickname: 'Loading',
  uiAccess: false,
}

export function UserCard({id, username, is_disabled, nickname, uiAccess}: User) {
  const { t } = useTranslation();

  const [updateUser] = useUpdateUserMutation();
  const [deleteUser] = useDeleteUserMutation();

  const { toast } = useToast();

  const form = useForm<z.infer<typeof userFormSchema>>({
    resolver: zodResolver(userFormSchema),
    defaultValues: defaultValues,
    values: {
      username,
      is_disabled,
      nickname,
      uiAccess,
    }
  });

  async function onSubmit(values: z.infer<typeof userFormSchema>){
    try {
      const updateTry = await updateUser({id, ...values}).unwrap()
      if(updateTry){
        toast({
          title: "Successfully updated.",
          description: "User successfully updated!"
        })
      }
    } catch (error) {
      console.log(error);
      toast({
        title: "Error",
        description: "Error while updating the user information.",
        variant: "destructive",
      })
    }
  }

  async function onDelete(event: SyntheticEvent) {
    console.log(event)
    event.preventDefault()
    try {
      if(id){
        const deleteTry = await deleteUser(id).unwrap()
        console.log(deleteTry);
        if(deleteTry){
          toast({
            title: "Successfully deleted.",
            description: "User successfully deleted!"
          })
        }
      }
    } catch (error) {
      console.log(error);
      toast({
        title: "Error",
        description: "Error while deleting the user.",
        variant: "destructive",
      })
    }
  }

  return (
    <>
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)}>
        <Card className="">
          <CardHeader>
            <CardTitle>{username}</CardTitle>
            <CardDescription>{nickname ? nickname : t('users.empty-nickname')}</CardDescription>
          </CardHeader>
          <CardContent>
            <FormField
              control={form.control}
              name="uiAccess"
              render={({ field }) => (
                <FormItem>
                  <FormLabel className="mr-5">{t('users.ui-access.name')}<InfoTooltip info={t('users.ui-access.description')}/></FormLabel>
                  <FormControl>
                    <Switch id="uiaccess" checked={field.value} onCheckedChange={field.onChange}></Switch>
                  </FormControl>
                </FormItem>
              )}
            />
          </CardContent>
            <CardFooter className="flex justify-between">
              {id ? <Button variant="destructive" onClick={onDelete}>{t('delete')}</Button> : <></>}
              <Button>{t('update')}</Button>
            </CardFooter>
          </Card>
        </form>
      </Form>
    </>
  )
}