import { Button } from './ui/button'
import { useTranslation } from 'react-i18next'
import { useEffect, useState } from 'react'
import { Popover, PopoverContent, PopoverTrigger } from './ui/popover';
import { Check, ChevronsUpDown } from 'lucide-react';
import { Command, CommandEmpty, CommandGroup, CommandInput, CommandItem } from './ui/command';
import { cn } from '@/lib/utils';
import { selectCurrentLocale, setLocale } from '@/features/settings/settingsSlice';
import { useAppDispatch, useAppSelector } from '@/hooks/store';

const availableLanguages = [
  { label: "language.german", value: "de" },
  { label: "language.english", value: "en" },
] as const


export function LanguageSwitch() {
  const dispatch = useAppDispatch()
  const [open, setOpen] = useState(false)
  const [init, setInit] = useState(false)

  const { t, i18n: {changeLanguage} } = useTranslation();
  const currentLocale = useAppSelector(selectCurrentLocale);
  
  useEffect(() => {
    changeLanguage(currentLocale)
    setInit(true);
  }, [init])

  function handleChangeLanguage(selectedValue: string) {
    changeLanguage(selectedValue);
    dispatch(setLocale({locale: selectedValue}));
    setOpen(false);
  }

  function findCurrentLanguage() {
    const current = availableLanguages.find((language) => language.value === currentLocale)?.label
    if(!current) {
      return  "language.english"
    }
    return current;
  }

  return (
    <>
      <Popover open={open} onOpenChange={setOpen}>
        <PopoverTrigger asChild>
          <Button
            variant="outline"
            role="combobox"
            aria-expanded={open}
            className="w-[100px] justify-between"
          >
            {currentLocale
              ? t(findCurrentLanguage())
              : "Select language..."}
            <ChevronsUpDown className="ml-2 h-4 w-4 shrink-0 opacity-50" />
          </Button>
        </PopoverTrigger>
        <PopoverContent className="w-[200px] p-0">
          <Command>
            <CommandInput placeholder={t('searchLanguage')} />
            <CommandEmpty>{t('noLanguageFound')}</CommandEmpty>
            <CommandGroup>
              {availableLanguages.map((language) => (
                <CommandItem
                  key={language.value}
                  value={language.value}
                  onSelect={(currentSelected) => handleChangeLanguage(currentSelected)}
                >
                  <Check
                    className={cn(
                      "mr-2 h-4 w-4",
                      currentLocale === language.value ? "opacity-100" : "opacity-0"
                    )}
                  />
                  {t(language.label)}
                </CommandItem>
              ))}
            </CommandGroup>
          </Command>
        </PopoverContent>
      </Popover>
    </>
  )
}