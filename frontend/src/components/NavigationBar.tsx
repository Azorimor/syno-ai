import { Link, NavLink } from 'react-router-dom'
import logo from '../assets/logo.png'
import { useTranslation } from 'react-i18next'
import { UserAvatar } from './UserAvatar'
import { useAppSelector } from '@/hooks/store'
import { buttonVariants } from './ui/button'

export function NavigationBar() {
  const user = useAppSelector((state) => state.auth.user)
  const { t } = useTranslation();
  
  return (
    <>
      <div className='flex flex-row bg-slate-200 dark:bg-gray-600'>
        <div className="basis-3/4 flex flex-row items-center">
          <Link to='/'>
            <img src={logo} alt="Syno AI Logo" className='w-16'/>
          </Link>
          <div className={user ? "flex flex-row gap-2" : "hidden"}>
            <NavLink to="/dashboard">{t('navigation.dashboard')}</NavLink>
            <NavLink to="/settings">{t('navigation.settings')}</NavLink>
            <NavLink to="/users">{t('navigation.users')}</NavLink>
            <NavLink to="/channels">{t('navigation.channels')}</NavLink>
          </div>
        </div>
        <div className="flex basis-1/4 flex-row items-center gap-2">
          {user 
            ?
            <Link to="/profile" className='ml-auto pr-4'>
              <UserAvatar/>
            </Link>
            :<Link className={buttonVariants({ variant: "default" }).concat(' ml-auto')} to="/login">{t('login.login')}</Link>
          }
        </div>
      </div>
    </>
  )
}