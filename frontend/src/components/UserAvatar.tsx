import { Avatar, AvatarFallback, AvatarImage } from "./ui/avatar";
import { createAvatar } from '@dicebear/core'
import { bottts } from '@dicebear/collection'
import { useAppSelector } from "@/hooks/store";


export function UserAvatar() {
  const username = useAppSelector((state) => state.auth.user?.username)

  const avatar = createAvatar(bottts, {
    seed: username
  });

  return (
    <div>
      <Avatar>
        <AvatarImage src={avatar.toDataUriSync()} alt="user profile icon"/>
        <AvatarFallback>{username?.slice(0,2).toUpperCase()}</AvatarFallback>
      </Avatar>
    </div>
  )
}