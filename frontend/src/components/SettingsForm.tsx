import { Button } from "@/components/ui/button";
import { useForm } from "react-hook-form";
import * as z from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { Switch } from "@/components/ui/switch";
import { useGetSettingsQuery, useUpdateSettingsMutation } from "@/services/settings";
import { Loading } from "./Loading";
import { useToast } from "./ui/use-toast";
import { Textarea } from "./ui/textarea";
import { cn } from "@/lib/utils";
import { Popover, PopoverContent, PopoverTrigger } from "./ui/popover";
import { Command, CommandEmpty, CommandGroup, CommandInput, CommandItem } from "./ui/command";
import { Check, ChevronsUpDown } from "lucide-react";
import { InfoTooltip } from "./InfoTooltip";
import { useTranslation } from "react-i18next";

const availableAIModels = [
  { label: "GPT-3.5 Turbo", value: "gpt-3.5-turbo" },
  { label: "GPT-4", value: "gpt-4" },
  { label: "GPT-4 32k", value: "gpt-4-32k"}
] as const

const settingsFormSchema = z.object({
  fakeUsername: z.string(),
  fakePassword: z.string(),
  sslEnabled: z.boolean(),
  sslCert: z.string().nonempty(),
  sslKey: z.string().nonempty(),
  synoToken: z.string().nonempty(),
  openAIToken: z.string().nonempty(),
  synoUrl: z.string().nonempty(),
  systemMessage: z.string().nonempty(),
  postOldContext: z.preprocess((a) => {
    if(typeof a === 'number') {
      return a;
    } else if(typeof a === 'string') {
      return parseInt(a,10);
    }
    return 1;
  },
  z.number().int().positive().max(100)),
  postMaxTokens: z.preprocess((a) => {
    if(typeof a === 'number') {
      return a;
    } else if(typeof a === 'string') {
      return parseInt(a,10);
    }
    return 1;
  },
  z.number().int().positive().max(5000)),
  isSetup: z.boolean().default(true),
  openAiModel: z.string().default('gpt-3.5-turbo'),
});

type SettingsFormValues = z.infer<typeof settingsFormSchema>

const defaultValues: Partial<SettingsFormValues> = {
  sslEnabled: false,
  sslCert: "",
  sslKey: "",
  synoToken: "",
  openAIToken: "",
  synoUrl: "",
  postOldContext: 1,
  postMaxTokens: 100,
  isSetup: true,
  fakeUsername: "",
  fakePassword: "",
  systemMessage: "You are a helpful assistant.",
  openAiModel: "gpt-3.5-turbo"
}

function SettingsForm() {
  const [update] = useUpdateSettingsMutation()
  const {data, isLoading} = useGetSettingsQuery()
  const { toast } = useToast()
  const { t } = useTranslation()
  
  const form = useForm<z.infer<typeof settingsFormSchema>>({
    resolver: zodResolver(settingsFormSchema),
    defaultValues: defaultValues,
    values: data
  });
  
  async function onSubmit(values: z.infer<typeof settingsFormSchema>) {
    try {
      const updateTry = await update(values).unwrap()
      console.log('updateTry', updateTry)
      if(updateTry){
        toast({
          title: "Successfully updated.",
          description: "Successfully updated settings."
        })
      }
    } catch (error) {
      console.log(error)
      toast({
        title: "Error",
        description: "Error while updating the settings.",
        variant: "destructive"
      })
    }
  }

  async function onErrors(err: object){
    console.log('FormValues: ',form.getValues())
    console.log('Error: ',err);
    console.log('Data: ',data);
  }
  return (
    <>
    {!isLoading ? 
      <Form {...form}>
        <form onSubmit={form.handleSubmit(onSubmit, onErrors)} className="space-y-8 mb-4">
          <div className="flex flex-row gap-4">
            <div className="flex-1">
              <FormField
                control={form.control}
                name="fakeUsername"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>{t('settings.name.fake-username')}<InfoTooltip info={t('settings.description.fake-username')}/></FormLabel>
                    <FormControl>
                      <Input
                        id="fakeUsername"
                        type="text"
                        autoCapitalize="none"
                        autoComplete="text"
                        autoCorrect="off"
                        {...field}
                      />
                    </FormControl>
                  </FormItem>
                )}
              />
            </div>
            <div className="flex-1">
              <FormField
                control={form.control}
                name="fakePassword"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>{t('settings.name.fake-password')}<InfoTooltip info={t('settings.description.fake-password')}/></FormLabel>
                    <FormControl>
                      <Input
                        id="fakePassword"
                        type="password"
                        autoCapitalize="none"
                        autoComplete="password"
                        autoCorrect="off"
                        {...field}
                      />
                    </FormControl>
                  </FormItem>
                )}
              />
            </div>
          </div>
          
          <div className="flex flex-row gap-4">
            <div className="flex-1">
              <FormField
                control={form.control}
                name="synoToken"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>{t('settings.name.synology-bot-token')}<InfoTooltip info={t('settings.description.synology-bot-token')}/></FormLabel>
                    <FormControl>
                      <Input
                        id="synoToken"
                        type="text"
                        autoCapitalize="none"
                        autoComplete="text"
                        autoCorrect="off"
                        {...field}
                      />
                    </FormControl>
                  </FormItem>
                )}
              />
            </div>
            <div className="flex-1">
              <FormField
                control={form.control}
                name="synoUrl"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>{t('settings.name.synology-base-url')}<InfoTooltip info={t('settings.description.synology-base-url')}/></FormLabel>
                    <FormControl>
                      <Input
                        id="synoUrl"
                        type="text"
                        autoCapitalize="none"
                        autoComplete="text"
                        autoCorrect="off"
                        placeholder="https://localhost:5001/webapi/entry.cgi?api=SYNO.Chat.External&method=chatbot&version=2"
                        {...field}
                      />
                    </FormControl>
                  </FormItem>
                )}
              />
            </div>
          </div>

          <div className="flex flex-row gap-4">
            <div className="flex-1">
              <FormField
                control={form.control}
                name="openAIToken"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>{t('settings.name.openai-api-token')}<InfoTooltip info={t('settings.description.openai-api-token')}/></FormLabel>
                    <FormControl>
                      <Input
                        id="openAIToken"
                        type="text"
                        autoCapitalize="none"
                        autoComplete="text"
                        autoCorrect="off"
                        {...field}
                      />
                    </FormControl>
                  </FormItem>
                )}
              />
            </div>
            <div className="flex-1">
              <FormField
                control={form.control}
                name="openAiModel"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>{t('settings.name.openai-api-model')}<InfoTooltip info={t('settings.description.openai-api-model')}/></FormLabel>
                    <Popover>
                      <PopoverTrigger asChild>
                        <FormControl>
                          <Button
                            variant="outline"
                            role="combobox"
                            className={cn(
                              "w-full justify-between",
                              !field.value && "text-muted-foreground"
                            )}
                          >
                            {field.value
                              ? availableAIModels.find(
                                  (model) => model.value === field.value
                                )?.label
                              : "Select model"}
                            <ChevronsUpDown className="ml-2 h-4 w-4 shrink-0 opacity-50" />
                          </Button>
                        </FormControl>
                      </PopoverTrigger>
                      <PopoverContent className="w-full p-0">
                        <Command>
                          <CommandInput placeholder={t('searchModel')} />
                          <CommandEmpty>{t('noModelFound')}</CommandEmpty>
                          <CommandGroup>
                            {availableAIModels.map((model) => (
                              <CommandItem
                                value={model.label}
                                key={model.value}
                                onSelect={() => {
                                  form.setValue("openAiModel", model.value)
                                }}
                              >
                                <Check
                                  className={cn(
                                    "mr-2 h-4 w-4",
                                    model.value === field.value
                                      ? "opacity-100"
                                      : "opacity-0"
                                  )}
                                />
                                {model.label}
                              </CommandItem>
                            ))}
                          </CommandGroup>
                        </Command>
                      </PopoverContent>
                    </Popover>
                  </FormItem>
                )}
              />
            </div>
            <div className="flex-1">
              <FormField
                control={form.control}
                name="postOldContext"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>{t('settings.name.openai-api-maxoldposts')}<InfoTooltip info={t('settings.description.openai-api-maxoldposts')}/></FormLabel>
                    <FormControl>
                      <Input
                        id="postOldContext"
                        type="number"
                        {...field}
                      />
                    </FormControl>
                  </FormItem>
                )}
              />
            </div>
            <div className="flex-1">
              <FormField
                control={form.control}
                name="postMaxTokens"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>{t('settings.name.openai-api-maxtokens')}<InfoTooltip info={t('settings.description.openai-api-maxtokens')}/></FormLabel>
                    <FormControl>
                      <Input
                        id="postMaxTokens"
                        type="number"
                        {...field}
                      />
                    </FormControl>
                  </FormItem>
                )}
              />
            </div>
          </div>

          <FormField
            control={form.control}
            name="systemMessage"
            render={({ field }) => (
              <FormItem>
                <FormLabel>{t('settings.name.openai-api-defaultmsg')}<InfoTooltip info={t('settings.description.openai-api-defaultmsg')}/></FormLabel>
                <FormControl>
                  <Textarea
                    id="systemMessage"
                    autoComplete="text"
                    autoCapitalize="none"
                    autoCorrect="off"
                    {...field}
                    className="h-40"
                  />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />
          <div className="flex flex-row gap-4">
            <div className="flex-1">
              <FormField
                control={form.control}
                name="sslCert"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>{t('settings.name.ssl-cert')}<InfoTooltip info={t('settings.description.ssl-cert')}/></FormLabel>
                    <FormControl>
                      <Textarea
                        id="sslCert"
                        autoComplete="text"
                        autoCapitalize="none"
                        autoCorrect="off"
                        {...field}
                        className="h-40"
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </div>
            <div className="flex-1">
              <FormField
                control={form.control}
                name="sslKey"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>{t('settings.name.ssl-key')}<InfoTooltip info={t('settings.description.ssl-key')}/></FormLabel>
                    <FormControl>
                      <Textarea
                        id="sslKey"
                        autoCapitalize="none"
                        autoComplete="text"
                        autoCorrect="off"
                        {...field}
                        className="h-40"
                      />
                    </FormControl>
                  </FormItem>
                )}
              />
            </div>
          </div>

          <FormField
            control={form.control}
            name="sslEnabled"
            render={({ field }) => (
              <FormItem>
                <FormLabel className="mr-5">{t('settings.name.ssl-enabled')}<InfoTooltip info={t('settings.description.ssl-enabled')}/></FormLabel>
                <FormControl>
                  <Switch id="sslEnabled" checked={field.value} onCheckedChange={field.onChange}></Switch>
                </FormControl>
              </FormItem>
            )}
          />
          <Button type="submit">{t('save')}</Button>
        </form>
      </Form>
      : <Loading/>}
    </>
  );
}

export default SettingsForm;
