import { ChannelCard } from "@/components/ChannelCard";
import { Loading } from "@/components/Loading";
import { PageTitle } from "@/components/PageTitle";
import { useGetChannelsQuery } from "@/services/channel";
import { useTranslation } from "react-i18next";


function Channels() {
  const {data, isLoading} = useGetChannelsQuery()
  const { t } = useTranslation();

  return (
    <>
      <PageTitle title={t('navigation.channels')}/>
      {isLoading ? <Loading/> :
        <div className="flex flex-col space-y-4">
          <ChannelCard key={0} id={undefined} name={""} inToken={""} outToken={""}/>
          <div className="grid gap-4 grid-cols-2">
            {
              data?.map((channel) => {
              return <ChannelCard key={channel.id} {...channel}/>
            })}
          </div>
        </div>
      }
    </>
  )
}

export default Channels;