import { LanguageSwitch } from "@/components/LanguageSwitch";
import { PageTitle } from "@/components/PageTitle";
import { ThemeSelect } from "@/components/ThemeSelect";
import { Button } from "@/components/ui/button";
import { logoutUser } from "@/features/auth/authSlice";
import { useAppDispatch } from "@/hooks/store";
import { useLogoutMutation } from "@/services/auth";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router";


function Profile() {
  const dispatch = useAppDispatch()
  const navigate = useNavigate()
  const { t } = useTranslation()
  const [logout, {isLoading: isLogoutLoading}] = useLogoutMutation()
  
  async function onLogout() {
    try {
      dispatch(logoutUser())
      await logout({}).unwrap()
      navigate('/')
    } catch (error) {
      console.log(error);
    }
  }
  return (
    <>
      <PageTitle title={t('navigation.profile')}/>
      <div className="flex gap-4 flex-col">
        <div className="flex flex-col gap-4 items-center">
          <span>
            {t('profile.language')}
          </span>
          <LanguageSwitch/>
        </div>
        <div className="flex flex-col gap-4 items-center">
          <span>
            {t('profile.theme')}
          </span>
          <ThemeSelect/>
        </div>
        <div>
          <Button onClick={onLogout} disabled={isLogoutLoading}>{t('logout')}</Button>
        </div>
      </div>
    </>
  )
}

export default Profile;