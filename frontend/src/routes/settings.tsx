import { PageTitle } from "@/components/PageTitle";
import SettingsForm from "@/components/SettingsForm";
import { useTranslation } from "react-i18next";

function Settings() {
  const { t } = useTranslation();

  return (
    <>
      <PageTitle title={t('navigation.settings')}/>
      <SettingsForm/>
    </>
  )
}

export default Settings;