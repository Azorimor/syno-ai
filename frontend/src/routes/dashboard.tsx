import { PageTitle } from "@/components/PageTitle";
import { TokensPerDayGraph } from "@/components/analytics/TokensPerDayGraph";
import { useTranslation } from "react-i18next";


function Dashboard() {
  const { t } = useTranslation();

  return (
    <>
      <PageTitle title={t('navigation.dashboard')}/>
      <TokensPerDayGraph/>
    </>
  )
}

export default Dashboard;