import { Loading } from "@/components/Loading";
import { PageTitle } from "@/components/PageTitle";
import { UserCard } from "@/components/UserCard";
import { useGetUsersQuery } from "@/services/user";
import { useTranslation } from "react-i18next";


function Users() {
  const {data, isLoading} = useGetUsersQuery()
  const { t } = useTranslation();

  return (
    <>
      <PageTitle title={t('navigation.users')}/>
      {isLoading ? <Loading/> :
        <div className="grid gap-4 grid-cols-2">
          {
            data?.map((user) => {
            return <UserCard key={user.id} {...user}/>
          })
        }
        </div>
      }
    </>
  )
}

export default Users;