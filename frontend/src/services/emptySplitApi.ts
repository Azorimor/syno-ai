// Or from '@reduxjs/toolkit/query' if not using the auto-generated hooks
import { apiRootUrl } from '@/helper/values'
import { RootState } from '@/store'
import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

// initialize an empty api service that we'll inject endpoints into later as needed
export const emptySplitApi = createApi({
  baseQuery: fetchBaseQuery({
    baseUrl: apiRootUrl,
    prepareHeaders: (headers, { getState }) => {
      // By default, if we have a token in the store, let's use that for authenticated requests
      const token = (getState() as RootState).auth.token
      if (token) {
        headers.set('authorization', `Bearer ${token}`)
      }
      return headers
    },
    //credentials: 'include',
  }),
  tagTypes: ['Settings', 'Channel', 'Analytics', 'User'],
  endpoints: () => ({}),
})
