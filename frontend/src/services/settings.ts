import { emptySplitApi } from './emptySplitApi'

export interface Settings {
  sslCert: string
  sslKey: string
  sslEnabled: boolean
  synoToken: string
  openAIToken: string
  synoUrl: string
  postOldContext: number
  postMaxTokens: number
  isSetup: boolean,
  fakeUsername: string,
  fakePassword: string,
  systemMessage: string,
  openAiModel: string
}

export const settingsApi = emptySplitApi.injectEndpoints({
  endpoints: (builder) => ({
    updateSettings: builder.mutation<Settings, Settings>({
      query: (body) => ({
        url: '/settings',
        method: 'PATCH',
        body: body,
      }),
      invalidatesTags: ['Settings']
    }),
    getSettings: builder.query<Settings, void>({
      query: () => '/settings',
      transformResponse: (response: {data: Settings}) => {
        return response.data
      },
      providesTags: ['Settings']
    })
  }),
})

export const { useGetSettingsQuery, useUpdateSettingsMutation } = settingsApi;
