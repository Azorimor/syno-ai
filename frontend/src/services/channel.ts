import { emptySplitApi } from './emptySplitApi'

export interface Channel {
  id: number | undefined,
  name: string,
  inToken: string | null,
  outToken: string | null,
}

export const channelApi = emptySplitApi.injectEndpoints({
  endpoints: (builder) => ({
    updateChannel: builder.mutation<Channel, Partial<Channel>>({
      query: (body) => ({
        url: '/channel',
        method: 'POST',
        body: body,
      }),
      invalidatesTags: (_result, _error, arg) => [{
        type: 'Channel',
        id: arg.id
      }]
    }),
    getChannels: builder.query<Channel[], void>({
      query: () => '/channel',
      transformResponse: (response: {data: Channel[]}) => {
        return response.data
      },
      providesTags: (result) => 
        result
          ? [...result.map(({ id }) => ({ type: 'Channel' as const, id})), 'Channel']
          : ['Channel'],
    }),
    deleteChannel: builder.mutation<{message: string, id: number}, number>({
      query(id) {
        return {
          url: `/channel/${id}`,
          method: 'DELETE',
        }
      },
      invalidatesTags: (_result, _error, id) => [{
        type: 'Channel',
        id: id
      }]
    }),
  }),
})

export const { useGetChannelsQuery, useUpdateChannelMutation, useDeleteChannelMutation } = channelApi;
