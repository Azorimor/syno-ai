import { emptySplitApi } from './emptySplitApi'

export interface User {
  username: string
  user_id: number,
  is_disabled: boolean,
  nickname: string,
  dsm_uid: number,
}

export interface LoginResponse {
  token: string,
  message: string,
  user: User | null,
}

export interface LoginRequest {
  username: string,
  code: string,
}

export interface CodeRequest {
  username: string,
}

export interface CodeResponse {
  message: string,
  code: string,
}

export interface LogoutRequest {

}

export interface LogoutResponse {
  message: string,
}

export const api = emptySplitApi.injectEndpoints({
  endpoints: (builder) => ({
    login: builder.mutation<LoginResponse, LoginRequest>({
      query: (credentials) => ({
        url: 'auth/login',
        method: 'POST',
        body: credentials,
      }),
    }),
    requestCode: builder.mutation<CodeResponse, CodeRequest>({
      query: (credentials) => ({
        url: 'auth/code',
        method: 'POST',
        body: credentials,
      }),
    }),
    logout: builder.mutation<LogoutResponse, LogoutRequest>({
      query: () => ({
        url: 'auth/logout',
        method: 'POST',
      }),
    }),
  }),
})

export const { useLoginMutation, useRequestCodeMutation, useLogoutMutation } = api;
