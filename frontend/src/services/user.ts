import { emptySplitApi } from './emptySplitApi'

export interface User {
  id: number | undefined,
  username: string,
  user_id: number,
  dsm_uid: number,
  is_disabled: boolean,
  nickname: string,
  uiAccess: boolean,
}

export const userApi = emptySplitApi.injectEndpoints({
  endpoints: (builder) => ({
    updateUser: builder.mutation<User, Partial<User>>({
      query: (body) => ({
        url: `/user/${body.id}`,
        method: 'PATCH',
        body: body,
      }),
      invalidatesTags: (_result, _error, arg) => [{
        type: 'User',
        id: arg.id
      }]
    }),
    getUsers: builder.query<User[], void>({
      query: () => '/user',
      transformResponse: (response: {data: User[]}) => {
        return response.data
      },
      providesTags: (result) => 
        result
          ? [...result.map(({ id }) => ({ type: 'User' as const, id})), 'User']
          : ['User'],
    }),
    deleteUser: builder.mutation<{message: string, id: number}, number>({
      query(id) {
        return {
          url: `/user/${id}`,
          method: 'DELETE',
        }
      },
      invalidatesTags: (_result, _error, id) => [{
        type: 'User',
        id: id
      }]
    }),
  }),
})

export const {useUpdateUserMutation, useGetUsersQuery, useDeleteUserMutation} = userApi;
