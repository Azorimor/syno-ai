import { emptySplitApi } from './emptySplitApi'

export interface Analytics {

}

export interface TokensPerDay {
  day: string,
  totalTokens: number,
  promptTokens: number,
  completionTokens: number,
}

export const analyticsApi = emptySplitApi.injectEndpoints({
  endpoints: (builder) => ({
    getTokensPerDay: builder.query<TokensPerDay[], void>({
      query: () => '/analytics/tokensperday',
      providesTags: ['Analytics']
    })
  }),
})

export const { useGetTokensPerDayQuery } = analyticsApi;
