import { configureStore } from '@reduxjs/toolkit';
import { emptySplitApi } from './services/emptySplitApi';
import storage from 'redux-persist/lib/storage';
import { FLUSH, PAUSE, PERSIST, PURGE, REGISTER, REHYDRATE, persistReducer } from 'redux-persist';
import { appReducer } from './features/appReducer';
import persistStore from 'redux-persist/es/persistStore';

export const persistConfig = {
  key: 'root',
  version: 1,
  storage
}

const persistedReducer = persistReducer(persistConfig, appReducer)

export const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) => getDefaultMiddleware({
    serializableCheck: {
      ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
    },
  }).concat(emptySplitApi.middleware),
})

export const persistor = persistStore(store);

setTimeout(() => {
  if (shouldResetState(store.getState().auth.lastLogin)) {
    persistor.purge();
  }
}, 1000); 

function shouldResetState(lastUpdate: number) {
  const currentTime = new Date().getTime();
  const maxDuration = 12 * 60 * 60 * 1000;
  return currentTime - lastUpdate > maxDuration;
}

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
